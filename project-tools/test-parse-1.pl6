#!/usr/bin/env perl6;

use XML::Actions;
use Data::Dump;

my %TYPES;
my $base-type;

class XSD-Actions is XML::Actions::Work {

  # sub indent (@p, $adj = 0) { "\t" x (@p + $adj - 2) }

  # There is a tag by this name in class Any!
  method all (Array $path) {
  }

  sub getLastTags($p, $tn) {
    $p.grep( *.name eq $tn );
  }

  method attribute (
    Array $path,
    :$name,
    :$type,
    :$use
  ) {
    my $pn = getLastTags($path, 'complexType')[* - 1];
    $pn = $pn.parent unless $pn.attribs<name>;

    %TYPES{$pn.attribs<name>}<members>.push: {
      :$name,
      :$type,
      form => 'attribute',
      :$use
    } if $pn;
  }

  method element (
    Array $path,
    :$ref,
    :$name,
    :$type,
    :$substitutionGroup,
    :$minOccurs,
    :$maxOccurs,
    :$abstract,
  ) {
    my $pn = getLastTags($path, 'complexType');
    $pn = $pn[* - 1] if $pn;

    # This is special casing to handle the following:
    #  <complexType name="LanguageType">
    #  <sequence>
    #    <element name="Language" minOccurs="0" maxOccurs="unbounded">
    #      <complexType>
    #        <sequence>
    #          <element name="Name" type="string" />
    #          <element name="Type" type="string" minOccurs="0" />
    #          <element name="AudioFormat" type="string" minOccurs="0" />
    #        </sequence>
    #      </complexType>
    #    </element>
    #  </sequence>
    #  </complexType>
    # Is there a more elegant way?
    if $pn {
      $pn = $pn.parent unless $pn.attribs<name>;
    }

    if $path[* - 2].name ne 'schema' || $pn {
      my $classNode = $pn // $path[* - 2];
      %TYPES{$classNode.attribs<name>}<members>.push: {
        :$name,
        type       => $type // $name,
        form       => 'element',
        base-type  => ($substitutionGroup // ''),
        'use'      => ($minOccurs // 0) > 0,
        positional => ($maxOccurs // '') eq 'unbounded' || (?$maxOccurs // 0) > 0
      }
    }
  }

}

sub MAIN (
  Str $filename where *.lc.ends-with('.xsd')     #= Filename to process. Must be an .xsd file
) {
  my $action = XML::Actions.new( file => $filename );
  $action.process( actions => XSD-Actions.new );

  say Dump %TYPES;
}
