- When compiling XSD into classes, make note of the attributes and elements.
  If the class has ONLY attributes, a member MUST be added that applies
  the 'xml-simple-content' trait. For example, this XSD definition:

    <complexType name="DecimalWithUnits">
      <simpleContent>
        <extension base="decimal">
          <attribute name="Units" type="string" use="required" />
        </extension>
      </simpleContent>
    </complexType

  Would emit the following class definition:

    class DecimalWithUnits does XML::Class {
      has $.Units;
      has $.Val is xml-simple-content;  # Added, as per requirement
    }

  Which would map to this XML snippet:

    <ns2:Height Units="inches">1.00</ns2:Height>

  Which should deserialize into:

    Height.new( Units => 'inches', Val => 1.0 )

  Note, there are no limits to the number of attributes, but only ONE
  member should be marked 'is xml-simple-content'!
